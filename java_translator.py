def translate(json_str, lib_path):

    # load packages
    import sys
    sys.path.append(lib_path)

    # load key-value map for database schema
    # the schema is removed in this demo 
    # because it contains sensitive information
    table_map = {}
    #with open("label+table_code.txt") as f:
    #    for line in f:
    #        (key, val) = line.replace('\n', '').split(',')
    #        table_map[key] = val
    #table_map['__time'] = 'register_settlement.SETTLEMENT_DATE'

    import json
    import datetime

    # load input json string to dictionary
    def to_dict(input_str):
        return json.loads(input_str)

    # returns values from key map
    # if key does not exist, return original string
    def check_keys(dim):
        if dim.encode('utf-8') in table_map.keys():
            return table_map[dim.encode('utf-8')]
        elif dim in table_map.keys():
            return table_map[dim]
        else:
            return dim

    # check filter
    # if 'filter:' in json_str, then remove it
    def check_filter(input_str):
        if '"filter":' in input_str:
            return input_str.replace('"filter":', '')
        else:
            return input_str

    # transform unix timestamp to sql datetime format
    def format_date(timestamp):
        return datetime.datetime.fromtimestamp(timestamp / 1e3).strftime('%Y-%m-%d %H:%M:%S')

    # combine rule filters to sql query
    def js_eval(js_dict, s):
        if (('field' in js_dict) is False) and (('fields' in js_dict) is False):
            if js_dict['type'] == 'bound':
                if 'time' in js_dict['dimension']:
                    js_dict['lower'] = "'" + format_date(js_dict['lower']) + "'"
                    js_dict['upper'] = "'" + format_date(js_dict['upper']) + "'"

                if js_dict['lowerStrict'] is False:
                    low = ' > '
                else:
                    low = ' >= '
                if js_dict['upperStrict'] is False:
                    up = ' < '
                else:
                    up = ' <= '

                interval = '(' + check_keys(js_dict['dimension']) + low + str(js_dict['lower']) + ')'\
                    + ' and ' + '(' + check_keys(js_dict['dimension']) + up + str(js_dict['upper']) + ')'

                return '(' + s + interval + ')'

            elif js_dict['type'] == 'in':

                if type(js_dict['values'][0]) == unicode:
                    list_str = ','.join('"' + item + '"' for item in js_dict['values'])
                elif 'time' in js_dict['dimension']:
                    list_str = ','.join(format_date(item) for item in js_dict['values'])
                else:
                    list_str = ','.join(item for item in js_dict['values'])
                return '(' + s + check_keys(js_dict['dimension']) + ' ' + js_dict['type'] + ' ' \
                       + '(' + list_str + ')' + ')'

            else:
                return '(' + s + check_keys(js_dict['dimension']) + ' ' + js_dict['type'] + ' ' \
                       + str(js_dict['values'][0]) + str(js_dict['values'][1]) + ')'

        if 'field' in js_dict:
            return '(' + js_dict['type'] + ' ' + js_eval(js_dict['field'], s) + ')'

        if 'fields' in js_dict:
            exp = ''
            for i in range(len(js_dict['fields'])):
                if i == 0:
                    exp += js_eval(js_dict['fields'][i], s)
                else:
                    exp += ' ' + js_dict['type'] + ' ' + js_eval(js_dict['fields'][i], s)

            return '(' + exp + ')'

    json_dict = to_dict(check_filter(json_str.encode('utf-8')))
    sql_str = js_eval(json_dict, '')

    return sql_str
