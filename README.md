# README
This is a demo of the druid-SQL translator I wrote for development team that translates druid filter rules to standard SQL queries using Jython.  
I have included a sample druid filter *test.json* for illustration. In order to run the python script, you have to add both jar files to the project.  