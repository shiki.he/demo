import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.io.File;

import org.python.core.*;
import org.python.util.PythonInterpreter;

public class demo {
    public static void main(String[] args) throws IOException {
        // read json string from file
        String contents = new String(Files.readAllBytes(Paths.get("test.json")));

        // obtain python package directory
        String currentDirectory;
        File file = new File("");
        currentDirectory = file.getAbsolutePath() + "\\lib.jar\\Lib";

        // Python interpreter setup
        Properties props = new Properties();
        props.put("python.console.encoding", "UTF-8");
        props.put("python.security.respectJavaAccessibility", "false");
        props.put("python.import.site", "false");
        Properties preProps = System.getProperties();
        PythonInterpreter.initialize(preProps, props, new String[0]);

        // execute python interpreter
        PythonInterpreter interpreter = new PythonInterpreter();
        interpreter.execfile("./java_translator.py");
        PyFunction func = (PyFunction) interpreter.get("translate",
                PyFunction.class);

        PyUnicode s = new PyUnicode(contents);
        PyString directory = new PyString(currentDirectory);
        PyObject pyobj = func.__call__(s, directory);

        String output = "select * from table where " + pyobj.toString();
        System.out.println(output);
    }
}
